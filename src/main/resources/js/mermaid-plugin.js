AJS.toInit(function() {
    mermaid.initialize({
        startOnLoad:false
    });

    AJS.$(".mermaid").each(function(i, e) {
        var timer = setInterval(function() {
            if ($(e).is(":visible")) {
                mermaid.init(undefined, $(e));
                clearInterval(timer);
            }
        }, 300);
    });
});